import unittest

from BoardingCard.BoardingCard import BoardingCard
from Descriptors.BoardingCardsDescriptor import BoardingCardsDescriptor


class TestBoardingCardsDescriptor(unittest.TestCase):
    def setUp(self):
        self.cards = [BoardingCard('Madrid', 'Barcelona', '78A', BoardingCard.TripTypes.TRAIN, 'Sit in seat 45B.'),
                      BoardingCard('Barcelona', 'Gerona Airport', 'airport bus', BoardingCard.TripTypes.BUS,
                                   'No seat assignment.'),
                      BoardingCard('Gerona Airport', 'Stockholm', 'SK455', BoardingCard.TripTypes.FLIGHT,
                                   'Gate 45B, seat 3A. Baggage drop at ticket counter 344.'),
                      ]
        self.descriptor = BoardingCardsDescriptor(self.cards)

    def test_descriptor_output(self):
        result = self.descriptor.describe_trip()
        expected_result = ['Take train 78A from Madrid to Barcelona. Sit in seat 45B.',
'Take the airport bus from Barcelona to Gerona Airport. No seat assignment.',
'From Gerona Airport, take flight SK455 to Stockholm. Gate 45B, seat 3A. Baggage drop at ticket counter 344.',
'You have arrived at your final destination.'
]
        self.assertEqual(result, expected_result)