from typing import List

from BoardingCard.BoardingCard import BoardingCard


class BoardingCardsDescriptor:

    def __init__(self, boarding_cards: List[BoardingCard]):
        self.cards = boarding_cards

    @staticmethod
    def get_flight_description(card: BoardingCard) -> str:
        return f"From {card.pointA}, take flight {card.transport_name} to {card.pointB}. {card.additional_info}"

    @staticmethod
    def get_train_description(card: BoardingCard) -> str:
        return f"Take train {card.transport_name} from {card.pointA} to {card.pointB}. {card.additional_info}"

    @staticmethod
    def get_bus_description(card: BoardingCard) -> str:
        return f"Take the {card.transport_name} from {card.pointA} to {card.pointB}. {card.additional_info}"

    def get_description(self, card: BoardingCard) -> str:
        return getattr(BoardingCardsDescriptor, f"get_{card.tripType}_description")(card)

    def describe_trip(self) -> List[str]:
        result: List[str] = []
        for card in self.cards:
            result.append(self.get_description(card))
        result.append("You have arrived at your final destination.")
        return result
