from typing import List, Dict

from BoardingCard.BoardingCard import BoardingCard


class BoardingCardsSorter:

    def __init__(self, boarding_cards: List[BoardingCard]):
        self.cards: List[BoardingCard] = boarding_cards
        self.from_to: dict = dict()
        self.all_points: set = set()
        self.cards_by_destination: Dict[str, BoardingCard] = dict()
        self.dest_points_order: List[str] = []
        self.sorted_cards: List[BoardingCard] = []

    def sort(self) -> List[BoardingCard]:
        for card in self.cards:  # O(n)
            self.from_to[card.pointA] = card.pointB  # O(1)
            self.cards_by_destination[card.pointB] = card  # O(1)
            self.all_points.update((card.pointA, card.pointB))  # O(1)

        starting_point = next(iter(self.all_points.difference(set(self.cards_by_destination.keys()))))  # O(1) (in Python3)

        for _ in range(len(self.cards)):  # O(n)
            self.dest_points_order.append(self.from_to[starting_point])  # O(1)
            starting_point = self.from_to[starting_point]  # O(1)

        for point in self.dest_points_order:  # O(n)
            self.sorted_cards.append(self.cards_by_destination[point])

        return self.sorted_cards
