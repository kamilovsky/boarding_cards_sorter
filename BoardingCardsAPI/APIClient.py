from typing import List, Dict, Union

from BoardingCard.BoardingCard import BoardingCard
from Descriptors.BoardingCardsDescriptor import BoardingCardsDescriptor
from Sorter.BoardingCardsSorter import BoardingCardsSorter

APIReturnValue = Union[List[str], List[BoardingCard]]


class APIClient:
    def sort_boarding_cards(self, boarding_cards: List[BoardingCard], trip_description: bool = False) \
            -> Dict[str, APIReturnValue]:

        results: Dict[str, APIReturnValue] = dict()
        sorter: BoardingCardsSorter = BoardingCardsSorter(boarding_cards=boarding_cards)
        cards_sorted: List[BoardingCard] = sorter.sort()
        results.update(dict(cards=cards_sorted))
        if trip_description:
            descriptor: BoardingCardsDescriptor = BoardingCardsDescriptor(boarding_cards=cards_sorted)
            results.update(trip_description=descriptor.describe_trip())
        return results
