import unittest
from random import shuffle

from BoardingCard.BoardingCard import BoardingCard
from BoardingCardsAPI.APIClient import APIClient


class TestBoardingClassSorter(unittest.TestCase):

    def setUp(self):
        self.cards = [BoardingCard('Madrid', 'Barcelona', '78A', BoardingCard.TripTypes.TRAIN, 'Sit in seat 45B.'),
                 BoardingCard('Barcelona', 'Gerona Airport', 'airport bus', BoardingCard.TripTypes.BUS,
                              'No seat assignment.'),
                 BoardingCard('Gerona Airport', 'Stockholm', 'SK455', BoardingCard.TripTypes.FLIGHT,
                              'Gate 45B, seat 3A. Baggage drop at ticket counter 344.'),
                 BoardingCard('Stockholm', 'New York JFK', 'SK22', BoardingCard.TripTypes.FLIGHT,
                              'Gate 22, seat 7B. Baggage will we automatically transferred from your last leg.'),
                 ]
        self.sorted_cards_copy = self.cards.copy()
        shuffle(self.cards)
        self.client = APIClient()

    def test_output_description(self):
        expected_result =  ['Take train 78A from Madrid to Barcelona. Sit in seat 45B.',
                            'Take the airport bus from Barcelona to Gerona Airport. No seat assignment.',
                            'From Gerona Airport, take flight SK455 to Stockholm. Gate 45B, seat 3A. Baggage drop at ticket counter 344.',
                            'From Stockholm, take flight SK22 to New York JFK. Gate 22, seat 7B. Baggage will we automatically transferred from your last leg.',
                            'You have arrived at your final destination.']
        result = self.client.sort_boarding_cards(boarding_cards=self.cards, trip_description=True)
        self.assertEqual(result['trip_description'], expected_result)

    def test_output_no_description(self):
        with self.assertRaises(KeyError):
            result = self.client.sort_boarding_cards(boarding_cards=self.cards, trip_description=False)
            result['trip_description']

    def test_output_sorted_list(self):
        result = self.client.sort_boarding_cards(boarding_cards=self.cards, trip_description=False)
        sorted_cards = result['cards']
        self.assertEqual(len(sorted_cards), len(self.cards))
        self.assertEqual(self.sorted_cards_copy, sorted_cards)
