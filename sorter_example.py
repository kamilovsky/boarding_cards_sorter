import random
from typing import Dict

from BoardingCard.BoardingCard import BoardingCard
from BoardingCardsAPI.APIClient import APIClient, APIReturnValue


def main():
    cards = [BoardingCard('Madrid', 'Barcelona', '78A', BoardingCard.TripTypes.TRAIN, 'Sit in seat 45B.'),
             BoardingCard('Barcelona', 'Gerona Airport', 'airport bus', BoardingCard.TripTypes.BUS, 'No seat assignment.'),
             BoardingCard('Gerona Airport', 'Stockholm', 'SK455', BoardingCard.TripTypes.FLIGHT, 'Gate 45B, seat 3A. Baggage drop at ticket counter 344.'),
             BoardingCard('Stockholm', 'New York JFK', 'SK22', BoardingCard.TripTypes.FLIGHT, 'Gate 22, seat 7B. Baggage will we automatically transferred from your last leg.'),
            ]

    random.shuffle(cards)

    client: APIClient = APIClient()
    result: Dict[str, APIReturnValue] = client.sort_boarding_cards(cards, trip_description=True)
    print("\n".join(result.get('trip_description', '')))


if __name__ == '__main__':
    main()