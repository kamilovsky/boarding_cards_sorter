# Boarding Cards Sorter
## Table of contents
- Introduction
  * Purpose
- Installation
  * Python version
  * Installing required Python packages
  * Updating Python packages
  * Running static types check
- Usage
  * Using API
  * Example usage
  * Tests

## Introduction

### Purpose

This API allows you to sort Boarding Cards in an order that allows you to travel
from point A to point B using all of the Boarding Cards, as long as there is always an unbroken chain between all the legs
of the trip. i.e. it's one continuous trip with no interruptions.


## Installation

### Python version

In order to use this software, ***Python >=3.6*** is required.
  
### Installing required Python packages

There are no mandatory third-party python packages that you need to install in order
to be able to use this software, however you can install mypy for static type checking
if you want to. To do so, run the command below:

```
pip install -r requirements.txt
```

### Updating Python packages

If you find yourself in need of upgrading some of the dependencies you can do so by running:
```
pip-compile --output-file requirements.txt requirements.in
```

### Running static types check
To run static types check simply run the command below in the main directory:
```python
mypy BoardingCard/ BoardingCardsAPI/ Descriptors/ Sorter/ tests/

```

## Usage

### Using API

To use this API you have to instantiate a *BoardingCardsAPI.APIClient.APIclient*
object, for example:
```python
from BoardingCardsAPI.APIClient import APIClient
client = APIClient()
```

The *__init__()* method takes no arguments, but the API exposes a *sort_boarding_cards*
method that takes a List of **BoardingCard**s and a **trip_description** bool as arguments.

The first argument is simply a List of **BoardingCard**s to sort, and the second
argument indicates whether we want the API to return just a sorted list of card, or
should the **trip_description** be returned  as well.

The **BoardingCard** object is structured like this:
```python
BoardingCard:
    pointA(str)
    pointB(str)
    transport_name(str)
    tripType(Enum)
    additional_info(str)
```

Once we have a List of **BoardingCard**s we can simply call the sorting function to
receive our sorted list back, just like this:
```python
result = client.sort_boarding_cards(cards, trip_description=True)
```

### Example usage

```python
import random
from typing import Dict

from BoardingCard.BoardingCard import BoardingCard
from BoardingCardsAPI.APIClient import APIClient, APIReturnValue


def main():
    cards = [BoardingCard('Madrid', 'Barcelona', '78A', BoardingCard.TripTypes.TRAIN, 'Sit in seat 45B.'),
             BoardingCard('Barcelona', 'Gerona Airport', 'airport bus', BoardingCard.TripTypes.BUS, 'No seat assignment.'),
             BoardingCard('Gerona Airport', 'Stockholm', 'SK455', BoardingCard.TripTypes.FLIGHT, 'Gate 45B, seat 3A. Baggage drop at ticket counter 344.'),
             BoardingCard('Stockholm', 'New York JFK', 'SK22', BoardingCard.TripTypes.FLIGHT, 'Gate 22, seat 7B. Baggage will we automatically transferred from your last leg.'),
            ]

    random.shuffle(cards)

    client: APIClient = APIClient()
    result: Dict[str, APIReturnValue] = client.sort_boarding_cards(cards, trip_description=True)
    print("\n".join(result.get('trip_description', '')))
```

The code above should produce a result like the one below:

```python
Take train 78A from Madrid to Barcelona. Sit in seat 45B.
Take the airport bus from Barcelona to Gerona Airport. No seat assignment.
From Gerona Airport, take flight SK455 to Stockholm. Gate 45B, seat 3A. Baggage drop at ticket counter 344.
From Stockholm, take flight SK22 to New York JFK. Gate 22, seat 7B. Baggage will we automatically transferred from your last leg.
You have arrived at your final destination.

```

#### Tests
To run the tests, run the command shown below in the main directory:
```
python -m unittest discover
```

It should produce the result similar to the one below:
```python
Ran 5 tests in 0.001s

OK

```
