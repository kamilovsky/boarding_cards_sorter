class BoardingCard:
    class TripTypes:
        FLIGHT = 'flight'
        TRAIN = 'train'
        BUS = 'bus'

    def __init__(self, point_a: str, point_b: str, transport_name: str, trip_type: TripTypes, additional_info: str):
        self.pointA: str = point_a
        self.pointB: str = point_b
        self.transport_name: str = transport_name
        self.tripType: BoardingCard.TripTypes = trip_type
        self.additional_info: str = additional_info

    def __str__(self):
        return f"Boarding Card ({self.pointA}, {self.pointB} ({self.tripType}))"

    def __repr__(self):
        return f"Boarding Card ({self.pointA}, {self.pointB} ({self.tripType}))"
