import unittest

from BoardingCard.BoardingCard import BoardingCard


class TestBoardingCard(unittest.TestCase):

    def setUp(self):
        self.card = BoardingCard('Madrid', 'Barcelona', '78A', BoardingCard.TripTypes.TRAIN, 'Sit in seat 45B.')

    def test_card_str(self):
        self.assertEqual(str(self.card), 'Boarding Card (Madrid, Barcelona (train))')